# CALIBRATION README # 

# Dependencies:
1) OpenCV is used for Intrinsic calibration
2) handeye_calib_camodocal for the hand eye calibration (already included):
	-Internal dependency of gflags (installation_script in scrpts folder)
	-Internal dependency of glogs (installation_script in scripts folder)
	-Internal dependency of ceres (already included)


# Compilation:

1) First run the install_dependencies.sh script in the scripts.
2) Navigate to the calibration directory.
3) Run catkin_make -DCATKIN_BLACKLIST_PACKAGES="ceres-solver"

