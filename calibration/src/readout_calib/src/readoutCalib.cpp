#include "readoutCalib.hpp"



bool readoutCalib::openBagFile(std::string filename) {
    std::ifstream file;
    file.open(filename.c_str());
    if( file.good() )
    {
        bagFile.open(filename, rosbag::BagMode::Read);
        return true;
    } else {
        std::cout << "Bag file could not be found" << std::endl;
        return false;
    }
}



void readoutCalib::getReadoutRate(std::string topic) {
	 std::vector<std::string> imageTopics;
    std::cout << "Image topic: " << topic << std::endl;
    imageTopics.push_back(topic);
    cv::Mat img, undistortedImg;
    rosbag::View view(bagFile, rosbag::TopicQuery(imageTopics));
    int counter = 0;
    for(rosbag::MessageInstance const m : view) {
        std::cout << "Topic name: " << m.getTopic() << std::endl;
        sensor_msgs::ImageConstPtr imgMsgPtr = m.instantiate<sensor_msgs::Image>();
        std::cout << "Timestamp: " << imgMsgPtr->header.stamp << std::endl;
        img = cv_bridge::toCvCopy(imgMsgPtr)->image;
		cv::namedWindow("cyclops image", cv::WINDOW_AUTOSIZE);
		cv::imshow("cyclops image", img );
        cv::waitKey(0);
    }
}






