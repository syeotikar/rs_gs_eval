#include "readoutCalib.hpp"

int main(int argc, char *argv[]) {
    if( argc != 6 ) {
        std::cout << "Not enough input arguments provided!" << std::endl;
        std::cout << "Format: " << std::endl;
        std::cout << "rosrun calibrate calibrateCam [bag file folder location] [cameraid] [image_topic_name] [led on time] [exposure]" << std::endl; 
    } else {
		std::string filename(argv[1]);
        std::string cameraId(argv[2]);
		std::string topic(argv[3]);
        std::string onTime(argv[4]);
		std::string exposure(argv[5]);
		readoutCalib cal;

		if( cal.openBagFile(filename) ) {
            if(topic.find("downward") != std::string::npos ) {
                cal.cameraName = "downward" + cameraId;
            } else if(topic.find("hires") != std::string::npos ) {
                cal.cameraName = "hires" + cameraId;
            } else if(topic.find("ir") != std::string::npos ) {
                cal.cameraName = "tof" + cameraId;
            }
        }
		cal.exposure = static_cast<double>(std::stoi(exposure));
		cal.ledOnTime = static_cast<double>(std::stoi(onTime));
		cal.getReadoutRate(topic);
    }
}
        
