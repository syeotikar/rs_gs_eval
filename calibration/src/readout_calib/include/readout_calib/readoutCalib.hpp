#include <opencv2/opencv.hpp>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <string>
#include <vector>
#include "rosbag/view.h"
#include "sensor_msgs/Image.h"
#include "cv_bridge/cv_bridge.h"




class readoutCalib {
 public:
	std::vector<cv::Mat> imageVector;

	std::string cameraName;

  	rosbag::Bag bagFile;
	
	double exposure;

	double ledOnTime;

	double fps;


public:

	bool openBagFile(std::string filename);


	void getReadoutRate(std::string topic);


	float getStripLength(cv::Mat img);
};

	
