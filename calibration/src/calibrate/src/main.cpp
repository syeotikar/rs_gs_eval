#include "calibrateCam.hpp"
#include <iostream>

int main(int argc, char *argv[]) {
    if( argc != 4 ) {
        std::cout << "Not enough input arguments provided!" << std::endl;
        std::cout << "Format: " << std::endl;
        std::cout << "rosrun calibrate calibrateCam [bag file folder location] [image_topic_name] [camera calibration skip frames number]" << std::endl; 
    } else {
        calibrateCam cam;
        std::string filename(argv[1]);
        std::string topic(argv[2]);
        std::string skip(argv[3]);
        if( cam.openBagFile(filename) ) {
            if(topic.find("downward") != std::string::npos ) {
                cam.cameraName = "downward";
            } else if(topic.find("hires") != std::string::npos ) {
                cam.cameraName = "hires";
            } else if(topic.find("ir") != std::string::npos ) {
                cam.cameraName = "tof";
            }
            int skipRate = std::stoi(skip);
            if( skipRate <= 0 ) {
                std::cout << "Invalid skip rate" << std::endl;
            }
            std::cout << "Opened file: " << filename << std::endl; 
            std::cout << "Opening bag file" << std::endl;
            cam.checkerboardSize = cv::Size(11,8);
            cam.checkSize = 0.06;
            cam.appendCheckerboardPoints(topic, skipRate);
            cam.performIntrinsicCalibration();
            cam.computeReprojectionError();
            cam.showUndistortedImages(topic);
            cam.saveIntrinsicData(filename);
            
            cam.getGroundtruthData(filename);
            cam.findBoardExtrinsics();

            cam.performHandEyeCalibration();
            cam.saveHandEyeData(filename);
            cam.bagFile.close();

        } else {
            std::cout << "Could not open bagfile " << std::endl;
            return 0;
        }
    }
}
