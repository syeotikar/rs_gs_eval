#include <fstream>
#include <iostream>
#include "sensor_msgs/Image.h"
#include "cv_bridge/cv_bridge.h"
#include "rosbag/view.h"
#include "calibrateCam.hpp"
#include "tf/tfMessage.h"
#include "tf/tf.h"
#include "tf_conversions/tf_eigen.h"
#include <Eigen/Core>
#include "camodocal/calib/HandEyeCalibration.h"
#include <fstream>
#include "boost/filesystem.hpp"


template <typename Input>
Eigen::Vector3d calibrateCam::eigenRotToEigenVector3dAngleAxis(Input eigenQuat) {
    Eigen::AngleAxisd ax3d(eigenQuat);
    return ax3d.angle() * ax3d.axis();
}

bool calibrateCam::openBagFile(std::string filename) {
    filename += "data.bag";
    std::ifstream file;
    file.open(filename.c_str());
    bagFile.open(filename, rosbag::BagMode::Read);
    return true;
}


void calibrateCam::displayImage(std::string topicName, const cv::Mat &img ) {
    cv::namedWindow(topicName, cv::WINDOW_AUTOSIZE);
    cv::imshow(topicName, img);
    cv::waitKey(5);
}

void calibrateCam::appendCheckerboardPoints(std::string topic, int skipRate) {
    cv::TermCriteria criteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 60, 1e-6);

    std::vector<std::string> imageTopics;
    std::cout << "Image topic: " << topic << std::endl;
    imageTopics.push_back(topic);
    cv::Mat img;
    rosbag::View view(bagFile, rosbag::TopicQuery(imageTopics));
    int counter = 0;
    for(rosbag::MessageInstance const m : view) {
        //std::cout << "Topic name: " << m.getTopic() << std::endl;
        sensor_msgs::ImageConstPtr imgMsgPtr = m.instantiate<sensor_msgs::Image>();
        //std::cout << "Timestamp: " << imgMsgPtr->header.stamp << std::endl;
        img = cv_bridge::toCvCopy(imgMsgPtr)->image;
        std::vector<cv::Point2f> corners;
        std::cout << "finding chessboard corners for image at: " << imgMsgPtr->header.stamp << std::endl;
        bool patternFound = cv::findChessboardCorners(img, checkerboardSize, corners);
        if( patternFound ) {
            cv::cornerSubPix(img,corners, cv::Size(3,3), cv::Size(-1,-1), criteria );
            cv::drawChessboardCorners(img, checkerboardSize, corners, patternFound);
            if( counter % skipRate == 0 ) {
                calibrationImagePoints.push_back(corners);
                pushbackCalibrationWorldPoints();
            }

            timeStamps.push_back(imgMsgPtr->header.stamp);
            imagePoints.push_back(corners);
            imageVector.push_back(img);
            pushbackWorldPoints();
            

            imgSize = cv::Size(img.rows, img.cols);
            std::cout << "Image rows: " << img.rows << std::endl;
            std::cout << "Image cols: " << img.cols << std::endl;
        }
        counter++;
        //displayImage(m.getTopic(), img);
    }
}

void calibrateCam::pushbackCalibrationWorldPoints() {
    std::cout << "Width: " << checkerboardSize.width << std:: endl;
    std::cout << "Height: " << checkerboardSize.height << std::endl;
    std::vector<cv::Point3f> points;
    float width = (checkerboardSize.width-1)*checkSize;
    float height = (checkerboardSize.height-1)*checkSize;
    for(int i = 0; i < checkerboardSize.height; i++ ) {
        for(int j = 0; j < checkerboardSize.width; j++ ) {
            cv::Point3f worldPoint;
            worldPoint.x = j * checkSize; 
            worldPoint.y = i * checkSize;
            worldPoint.z = 0;
            points.push_back(worldPoint);
        }
    }
    calibrationWorldPoints.push_back(points);
}


void calibrateCam::pushbackWorldPoints() {
    std::cout << "Width: " << checkerboardSize.width << std:: endl;
    std::cout << "Height: " << checkerboardSize.height << std::endl;
    std::vector<cv::Point3f> points;
    float width = (checkerboardSize.width-1)*checkSize;
    float height = (checkerboardSize.height-1)*checkSize;
    for(int i = 0; i < checkerboardSize.height; i++ ) {
        for(int j = 0; j < checkerboardSize.width; j++ ) {
            cv::Point3f worldPoint;
            worldPoint.x = j * checkSize; 
            worldPoint.y = i * checkSize;
            worldPoint.z = 0;
            points.push_back(worldPoint);
        }
    }
    worldPoints.push_back(points);
}

void calibrateCam::performIntrinsicCalibration() {
    cv::TermCriteria criteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 60, 1e-6);
    int flags = 0;
    flags |= CV_CALIB_RATIONAL_MODEL;                                                                       
    std::cout << "Calibrating camera ..." << std::endl;
    std::cout << "world points size: " << calibrationWorldPoints.size() << std::endl;
    std::cout << "image Points size: " << calibrationImagePoints.size() << std::endl;
    //cv::Mat fisheyeDistort, fisheyeRVecs, fisheyeTVecs;
    //fisheyeDistort = cv::Mat::zeros(1, 4, CV_64F);
    std::cout << "Initializing with Normal camera calibration.." << std::endl;
    cv::calibrateCamera(calibrationWorldPoints, calibrationImagePoints, imgSize, cameraMat, distortCoeffs, rVecs, tVecs, flags, criteria );
    // for(int i = 0; i < 4; i++ ) {
    //     fisheyeDistort.at<double>(0,i) = distortCoeffs.at<double>(0,i);
    // }
     std::cout << "Camera mat: " << cameraMat << std::endl;
    std::cout << "Distortion coeffs: " << distortCoeffs << std::endl;
    //std::cout << "Peforming fisheye calibration.. " << std::endl;
    //cv::fisheye::calibrate(worldPoints, imagePoints, imgSize, cameraMat, fisheyeDistort, fisheyeRVecs, fisheyeTVecs, flags, criteria );
    //cv::calibrateCamera(worldPoints, imagePoints, imgSize, cameraMat, distortCoeffs, rotVecs, tVecs );
    // std::cout << "fisheye Camera mat: " << cameraMat << std::endl;
    // std::cout << "fisheye Distortion coeffs: " << distortCoeffs << std::endl;
    // std::cout << "Fish eye distortion coeffs: " << fisheyeDistort << std::endl;
}

void calibrateCam::undistortAndDrawAxes( int index, cv::Mat rot, cv::Mat trans ) {
    cv::Mat undistortedImage;
    //cv::undistort(imageVector[index], undistortedImage, cameraMat, distortCoeffs);
    std::vector<cv::Point3f> axesPoints;
    std::cout << "Index of image is: " << index << std::endl;
    cv::Mat imagePoints;
    cv::Mat coloredImage;
    axesPoints.push_back(cv::Point3f(0,0,0));
    axesPoints.push_back(cv::Point3f(1,0,0));
    axesPoints.push_back(cv::Point3f(0,1,0));
    axesPoints.push_back(cv::Point3f(0,0,1));
    cv::cvtColor(imageVector[index], coloredImage, cv::COLOR_GRAY2BGR);
    cv::projectPoints(cv::Mat(axesPoints), rot, trans, cameraMat, distortCoeffs, imagePoints);
    cv::line(coloredImage, cv::Point2f(imagePoints.at<float>(0,0), imagePoints.at<float>(0,1)), 
                                cv::Point2f(imagePoints.at<float>(1,0), imagePoints.at<float>(1,1)), cv::Scalar(255,0,0), 2);
    cv::line(coloredImage, cv::Point2f(imagePoints.at<float>(0,0), imagePoints.at<float>(0,1)), 
                                cv::Point2f(imagePoints.at<float>(2,0), imagePoints.at<float>(2,1)), cv::Scalar(0,255,0), 2);
    cv::line(coloredImage, cv::Point2f(imagePoints.at<float>(0,0), imagePoints.at<float>(0,1)), 
                                cv::Point2f(imagePoints.at<float>(3,0), imagePoints.at<float>(3,1)), cv::Scalar(0,0,255), 2);
    cv::namedWindow("Axes", cv::WINDOW_AUTOSIZE);
    cv::imshow("Axes", coloredImage);
    cv::waitKey(5);



}




void calibrateCam::findBoardExtrinsics() {
    std::cout << "Computing board positions using pnp.." << std::endl;
    std::cout << "world points size: " << worldPoints.size() << std::endl;
    std::cout << "image points size: " << imagePoints.size() << std::endl;
    for(int i = 0; i < worldPoints.size(); i++ ) {
         std::cout << "Computing board position at: " << timeStamps[i] << std::endl;
        cv::Mat cvAffine = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat rotRod;
        cv::Mat trans;
        cv::Mat rot;
        std::vector<cv::Point2f> imagePoints2;
        //cv::solvePnP(worldPoints[i], imagePoints[i], cameraMat, distortCoeffs, rotRod, trans, false,);
        cv::solvePnP(worldPoints[i], imagePoints[i], cameraMat, distortCoeffs, rotRod, trans, false);
        //cv::solvePnPRansac(worldPoints[i], imagePoints[i], cameraMat, distortCoeffs, rotRod, trans, false, 10000, 2);
        cv::projectPoints(cv::Mat(worldPoints[i]), rotRod, trans, cameraMat, distortCoeffs, imagePoints2 );
        double err = cv::norm(cv::Mat(imagePoints[i]), cv::Mat(imagePoints2), CV_L2);
        int n = (int)worldPoints[i].size();
        double avgErr = std::sqrt(err*err/n);
        std::cout << "Reprojection error is: " << std::sqrt(err*err/n)  << std::endl;              
        //if( avgErr < 0.1 ) {
        cv::Rodrigues(rotRod, rot);
        for(int r = 0; r < 3; r++ ) {
            for(int c = 0; c < 3; c++ ) {
                cvAffine.at<double>(r,c) = rot.at<double>(r,c); 
            }
        }
        undistortAndDrawAxes(i, rot, trans);
        cvAffine.at<double>(0,3) = trans.at<double>(0,0);
        cvAffine.at<double>(1,3) = trans.at<double>(1,0);
        cvAffine.at<double>(2,3) = trans.at<double>(2,0);
        cvAffine.at<double>(3,3) = 1;
        Eigen::Affine3d affineEig;
        Eigen::Matrix4d matAffine;
        matAffine.setIdentity();
        matAffine = Eigen::Map<Eigen::Matrix4d>(reinterpret_cast<double*>(cvAffine.data),cvAffine.rows,cvAffine.cols);
        affineEig.matrix() = (matAffine.transpose()).inverse();
        camToBoard.push_back(affineEig);
        // } else {
        //     timeStamps.erase(timeStamps.begin() + i);
        // }
    }
}



double calibrateCam::computeReprojectionError() {
    std::vector<cv::Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    std::vector<float> perViewErrors;
    perViewErrors.resize(calibrationWorldPoints.size());
    std::cout << "Computing reprojection errors.. " << std::endl;
    for( i = 0; i < (int)calibrationWorldPoints.size(); ++i ) {
        cv::projectPoints( cv::Mat(calibrationWorldPoints[i]), rVecs[i], tVecs[i], cameraMat,  
                                            distortCoeffs, imagePoints2);
        err = cv::norm(cv::Mat(calibrationImagePoints[i]), cv::Mat(imagePoints2), CV_L2);              
        int n = (int)calibrationWorldPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err*err/n);                        
        totalErr        += err*err;                                       
        totalPoints     += n;
    }
    double error = std::sqrt(totalErr/totalPoints);
    std::cout << "Avg. reprojection error is: " << error << std::endl;
    reprojectionError = std::sqrt(totalErr/totalPoints);
    return std::sqrt(totalErr/totalPoints);              
}


void calibrateCam::getGroundtruthData(std::string fileName) {
    rosbag::View view(bagFile,  rosbag::TopicQuery("/tf"));
    tf::Transformer tfT = tf::Transformer(true, ros::Duration(360));
    std::string markerFrameName ="RigidBody";
    if( fileName.find("RS") != std::string::npos ) {
        markerFrameName += "_RS";
    }
    for( rosbag::MessageInstance const m : view) {
        tf::tfMessagePtr tfM = m.instantiate<tf::tfMessage>();
        for( auto trans : tfM->transforms ) {
            if (trans.header.frame_id == "world"){
                if (trans.child_frame_id == markerFrameName) {
                    tf::StampedTransform fromMessage;
                    tf::transformStampedMsgToTF(trans, fromMessage );
                    tfT.setTransform(fromMessage);
                }
            }
        }
    }
    for(int i = 0; i < timeStamps.size(); i++ ) {
        ros::Time stamp = timeStamps[i];
        tf::StampedTransform worldToRigidBody;
        if( tfT.canTransform("world", markerFrameName, stamp)) {
            tfT.lookupTransform("world", markerFrameName, stamp, worldToRigidBody);
            Eigen::Affine3d worldToMarkerEigen;
            tf::transformTFToEigen(worldToRigidBody, worldToMarkerEigen);
            std::cout << "Ground truth data: " << worldToMarkerEigen.translation().transpose() << std::endl;
            worldToMarker.push_back(worldToMarkerEigen);
        } else {
            std::cout << "Erasing cam to board transforms for time: " << stamp << std::endl;
            timeStamps.erase(timeStamps.begin() + i);
            worldPoints.erase(worldPoints.begin() + i);
            imagePoints.erase(imagePoints.begin() + i);
            imageVector.erase(imageVector.begin() + i);
            
        }
    }

}


void calibrateCam::showUndistortedImages(std::string topic) {
    std::vector<std::string> imageTopics;
    std::cout << "Image topic: " << topic << std::endl;
    imageTopics.push_back(topic);
    cv::Mat img, undistortedImg;
    rosbag::View view(bagFile, rosbag::TopicQuery(imageTopics));
    int counter = 0;
    for(rosbag::MessageInstance const m : view) {
        std::cout << "Topic name: " << m.getTopic() << std::endl;
        sensor_msgs::ImageConstPtr imgMsgPtr = m.instantiate<sensor_msgs::Image>();
        std::cout << "Timestamp: " << imgMsgPtr->header.stamp << std::endl;
        img = cv_bridge::toCvCopy(imgMsgPtr)->image;
        cv::undistort(img,undistortedImg, cameraMat, distortCoeffs);
        cv::namedWindow("Undistorted images", cv::WINDOW_AUTOSIZE);
        cv::imshow("Undistorted images", undistortedImg);
        cv::waitKey(5);
    }

}

void calibrateCam::performHandEyeCalibration() {

    auto t1_it = worldToMarker.begin();
    auto t2_it = camToBoard.begin();

    Eigen::Affine3d firstEEInverse, firstCamInverse;
    std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d>> tvecsArm, rvecsArm, tvecsFiducial, rvecsFiducial;

    bool firstTransform = true;

    for (int i = 0; i < worldToMarker.size(); ++i, ++t1_it, ++t2_it) {
        auto& eigenEE = *t1_it;
        auto& eigenCam = *t2_it;
        if (firstTransform) {
            firstEEInverse = eigenEE.inverse();
            firstCamInverse = eigenCam.inverse();
            ROS_INFO("Adding first transformation.");
            firstTransform = false;
        } else {
            Eigen::Affine3d robotTipinFirstTipBase = firstEEInverse * eigenEE;
            Eigen::Affine3d fiducialInFirstFiducialBase = firstCamInverse * eigenCam;
            rvecsArm.push_back(eigenRotToEigenVector3dAngleAxis(robotTipinFirstTipBase.rotation()));
            tvecsArm.push_back(robotTipinFirstTipBase.translation());
            rvecsFiducial.push_back(eigenRotToEigenVector3dAngleAxis(fiducialInFirstFiducialBase.rotation()));
            tvecsFiducial.push_back(fiducialInFirstFiducialBase.translation());
            ROS_INFO("Hand Eye Calibration Transform Pair Added");

            Eigen::Vector4d r_tmp = robotTipinFirstTipBase.matrix().col(3);
            r_tmp[3] = 0;
            Eigen::Vector4d c_tmp = fiducialInFirstFiducialBase.matrix().col(3);
            c_tmp[3] = 0;

            std::cout
                << "L2Norm EE: "
                << robotTipinFirstTipBase.matrix().block(0, 3, 3, 1).norm()
                << " vs Cam:"
                << fiducialInFirstFiducialBase.matrix().block(0, 3, 3, 1).norm()
                << std::endl;

             std::cout << "EE transform: \n" << eigenEE.matrix() << std::endl;
            std::cout << "Cam transform: \n" << eigenCam.matrix() << std::endl;
            std::cout << "Appending board position: " << i << " of " << worldPoints.size() << std::endl;
        }
    }
   
    camodocal::HandEyeCalibration calib;
    calib.estimateHandEyeScrew(rvecsArm, tvecsArm, rvecsFiducial, tvecsFiducial,
                            camToMarker, summary, false);
    Eigen::Transform<double, 3, Eigen::Affine> result(camToMarker);
    std::cout << "Translation (x,y,z) : "
            << result.translation().transpose() << std::endl;
    Eigen::Quaternion<double> quaternionResult(result.rotation());
    std::stringstream ss;
    ss << quaternionResult.w() << ", " << quaternionResult.x() << ", "
    << quaternionResult.y() << ", " << quaternionResult.z() << std::endl;
    std::cout << "Rotation (w,x,y,z): " << ss.str() << std::endl;
    Eigen::Transform<double, 3, Eigen::Affine> resultAffineInv =
        result.inverse();
    std::cout << "Inverted translation (x,y,z) : "
            << resultAffineInv.translation().transpose() << std::endl;
    quaternionResult = Eigen::Quaternion<double>(resultAffineInv.rotation());
    ss.clear();
    ss << quaternionResult.w() << " " << quaternionResult.x() << " "
    << quaternionResult.y() << " " << quaternionResult.z() << std::endl;
    std::cout << "Inverted rotation (w,x,y,z): " << ss.str() << std::endl;
    return;
}


void calibrateCam::saveIntrinsicData(std::string folderLocation) {
    boost::filesystem::path dir1(folderLocation + "calibrationResults");
    if( !(boost::filesystem::exists(dir1))){ 
        std::cout << "Creating calibration results  folder.." << std::endl;
        if (boost::filesystem::create_directory(dir1)) {
            std::cout << "....Successfully Created calibration results !" << std::endl;
        }
    }
    std::string location = folderLocation +  "calibrationResults/" + cameraName;
    boost::filesystem::path dir2(location);
    if( !(boost::filesystem::exists(dir2))){ 
        std::cout << "Creating " + cameraName + " folder.." << std::endl;
        if (boost::filesystem::create_directory(dir2)) {
            std::cout << "....Successfully Created calibration results !" << std::endl;
        }
    }
    std::string fileName = location + "//initrisicCalibration.txt";
    std::cout << "File location: " << fileName << std::endl;

    std::fstream outFile;
    outFile.open(fileName, std::fstream::out);
    outFile << "Camera matrix: " << std::endl;
    outFile << cameraMat << std::endl;
    outFile << "Distortion coefficients: " << std::endl;
    outFile << distortCoeffs << std::endl;
    outFile << "Reprojection error: " << std::endl;
    outFile <<  reprojectionError << std::endl;   
    outFile.close();
    return;
} 


void calibrateCam::saveHandEyeData(std::string folderLocation) {
    boost::filesystem::path dir1(folderLocation + "calibrationResults");
    if( !(boost::filesystem::exists(dir1))){ 
        std::cout << "Creating calibration results  folder.." << std::endl;
        if (boost::filesystem::create_directory(dir1)) {
            std::cout << "....Successfully Created calibration results !" << std::endl;
        }
    }
    std::string location = folderLocation +  "calibrationResults/" + cameraName;
    boost::filesystem::path dir2(location);
    if( !(boost::filesystem::exists(dir2))){ 
        std::cout << "Creating " + cameraName + " folder.." << std::endl;
        if (boost::filesystem::create_directory(dir2)) {
            std::cout << "....Successfully Created calibration results !" << std::endl;
        }
    }
    std::string fileName = location + "//markerToCam.txt";
    std::cout << "File location: " << fileName << std::endl;

    std::fstream outFile;
    outFile.open(fileName, std::fstream::out);
    outFile << "Rotation quaternion(w,x,y,z): " << std::endl;
    Eigen::Transform<double, 3, Eigen::Affine> result(camToMarker);
    Eigen::Quaternion<double> quaternionResult(result.rotation());
    std::cout << " Saving hand eye Translation (x,y,z) : "
                    << result.translation().transpose() << std::endl;
    outFile << quaternionResult.w() << ", " << quaternionResult.x() << ", " << quaternionResult.y() << ", " << quaternionResult.z() << std::endl;
    outFile << "Translation (x,y,z) : " << std::endl;
    outFile << result.translation().transpose() << std::endl;
    Eigen::Transform<double, 3, Eigen::Affine> resultAffineInv = result.inverse();
    outFile << "Inverted translation (x,y,z) : "
            << resultAffineInv.translation().transpose() << std::endl;
    quaternionResult = Eigen::Quaternion<double>(resultAffineInv.rotation());
    outFile << "Inverted rotation (w,x,y,z): " << std::endl;

    outFile << quaternionResult.w() << " " << quaternionResult.x() << " " << quaternionResult.y() << " " << quaternionResult.z() << std::endl;
    outFile << "initial_cost:" << summary.initial_cost << std::endl;
    outFile << "final_cost: " << summary.final_cost << std::endl;
    outFile << "change_cost: " << summary.initial_cost - summary.final_cost << std::endl;
    outFile << "termination_type: " << summary.termination_type << std::endl;
    outFile << "num_successful_iteration: " << summary.num_successful_steps << std::endl;
    outFile << "num_unsuccessful_iteration: " << summary.num_unsuccessful_steps << std::endl;
    outFile << "num_iteration: " << summary.num_unsuccessful_steps + summary.num_successful_steps << std::endl;
    outFile.close();
    return;


}