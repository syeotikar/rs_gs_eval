#include <opencv2/opencv.hpp>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <string>
#include <vector>
#include "Eigen/Core"
#include "Eigen/Eigen"
#include "ceres/ceres.h"
#include "ceres/types.h"


class calibrateCam {
 private:
  std::vector<cv::Mat> rVecs;

  std::vector<cv::Mat> tVecs;

  std::vector<std::vector<cv::Point3f> > calibrationWorldPoints;

  std::vector<std::vector<cv::Point2f> > calibrationImagePoints;

  std::vector<std::vector<cv::Point3f> > worldPoints;

  std::vector<std::vector<cv::Point2f> > imagePoints;



  cv::Mat cameraMat;

  cv::Mat distortCoeffs;

  std::vector<ros::Time> timeStamps;

  std::vector<Eigen::Affine3d, Eigen::aligned_allocator<Eigen::Affine3d>> camToBoard;

  std::vector<Eigen::Affine3d, Eigen::aligned_allocator<Eigen::Affine3d>> worldToMarker;

  std::vector<cv::Mat> imageVector;
  
  double reprojectionError;

  ceres::Solver::Summary summary;

 void undistortAndDrawAxes(int i, cv::Mat rot, cv::Mat trans);

 public:

  std::string cameraName;

  rosbag::Bag bagFile;

  Eigen::Matrix4d camToMarker;


  cv::Size checkerboardSize;

  double checkSize;

  cv::Size imgSize;

  void displayImage(std::string topicName, const cv::Mat &img);
    
  bool openBagFile(std::string filename);

  void appendCheckerboardPoints(std::string topic, int skipRate);

  void performIntrinsicCalibration();


  void pushbackWorldPoints();

  double computeReprojectionError();

  void getGroundtruthData(std::string filename);

  void performHandEyeCalibration();

  void showUndistortedImages(std::string topic);

  void saveIntrinsicData(std::string folderLocation);
  
  void saveHandEyeData(std::string folderLocation);

  void pushbackCalibrationWorldPoints();

  void findBoardExtrinsics();



  template <typename Input>
  Eigen::Vector3d eigenRotToEigenVector3dAngleAxis(Input eigenQuat);


};