#!/bin/bash

sudo apt-get install -y libgoogle-glog*
sudo apt-get install -y libgflags-dev
sudo apt-get install -y libatlas-base-dev
sudo apt-get update
sudo apt-get install libsuitesparse-dev
cd ../src
git clone https://ceres-solver.googlesource.com/ceres-solver
mkdir ceres-bin
cd ceres-bin
cmake ../ceres-solver
make -j3
make test
make install